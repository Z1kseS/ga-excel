import _ from 'lodash';

import {
  IterationResult,
  IterationResultStats
} from '../iteration-result.interface';

export type FileKey = Pick<IterationResult, 'evaluation'>;

export interface DimensionStatsSummary {
  max: Partial<IterationResultStats>;
  avg: Partial<IterationResultStats>;
}

export interface RunResult {
  key: {};
  dimensions: {
    [index: number]: DimensionStatsSummary;
  };
  three: DimensionStatsSummary;
  all: DimensionStatsSummary;
}

export interface FileResult {
  key: FileKey;
  data: RunResult[];
}

const nullFileFields: { [P in keyof FileKey]: null } = {
  evaluation: null
};

export const fileFields = _.keys(nullFileFields) as Array<keyof FileKey>;
