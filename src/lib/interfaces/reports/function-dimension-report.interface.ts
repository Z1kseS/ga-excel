import _ from 'lodash';

import {
  IterationResult,
  IterationResultStats
} from '../iteration-result.interface';

export type FileKey = Pick<IterationResult, 'dimension' | 'evaluation'>;

export interface RunResult {
  data: IterationResultStats[];
  key: {};
}

export interface FileResult {
  data: RunResult[];
  key: FileKey;
}

const nullFileFields: { [P in keyof FileKey]: null } = {
  dimension: null,
  evaluation: null
};

export const fileFields = _.keys(nullFileFields) as Array<keyof FileKey>;
