import _ from 'lodash';

export interface IterationResult {
  // run number
  run: number;

  // mandatory fields
  evaluation: string;
  dimension: number;

  fpr: number;
  lp: number;
  lpr: number;
  gp: number;
  gpr: number;
  np: number;
  pr: number;
  nfe: number;
  nSeeds: number;
  lfp: number;
  hfp: number;
}

export type IterationResultStats = Pick<
  IterationResult,
  | 'fpr'
  | 'lp'
  | 'lpr'
  | 'gp'
  | 'gpr'
  | 'np'
  | 'pr'
  | 'nfe'
  | 'nSeeds'
  | 'lfp'
  | 'hfp'
>;

const nullStatsFields: { [P in keyof IterationResultStats]: null } = {
  fpr: null,
  gp: null,
  gpr: null,
  hfp: null,
  lfp: null,
  lp: null,
  lpr: null,
  nSeeds: null,
  nfe: null,
  np: null,
  pr: null
};

export const statsFields = _.keys(nullStatsFields) as Array<
  keyof IterationResultStats
>;
