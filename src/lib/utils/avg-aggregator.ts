import _ from 'lodash';

export const avgAggregator = <T, P extends keyof T>(
  data: T[],
  key: P
): number | undefined => {
  return !data || !data.length
    ? void 0
    : _.mean(data.map(entry => entry[key]).filter(_.isNumber));
};

export const avgFieldsAggregator = <T, P extends keyof T>(
  data: T[],
  keys: P[]
): { [K in P]?: number } => {
  const stats: { [K in P]?: number } = {};

  _.forEach(keys, key => {
    stats[key] = avgAggregator(data, key);
  });

  return stats;
};
