import Excel from 'exceljs';
import _ from 'lodash';

import {
  ExcelBlockDefinition,
  ExcelContentBlockDefinition
} from './../block/excel-block-definition.interface';

// function toColumnName(num: number): string {
//   const ret = '';
//   for (let ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
//     ret = String.fromCharCode((num % b) / a) + 65 + ret;
//   }
//   return ret;
// }

const font = {
  // name: 'Comic Sans MS',
  size: 12
};

export class Composer<T> {
  constructor(
    private name: string,
    private headers: ExcelBlockDefinition[],
    private content: ExcelContentBlockDefinition,
    private footers: ExcelBlockDefinition[],
    private data: T[],
    private excelOptions: Partial<Excel.stream.xlsx.WorkbookWriterOptions> // private composerOptions
  ) {
    // this._additionalMerges = [];
  }

  // public _calculateFormulaRow(columns, startIndex) {
  //   if (_.get(this._composerOptions, 'formula')) {
  //     const {
  //       formulaLabel,
  //       formulaLabelFields,
  //       ignoreFields
  //     } = this._composerOptions;

  //     const formulaRow = _.chain(columns)
  //       .map(({ key }, index) => {
  //         const isLabelField = _.includes(formulaLabelFields, key);
  //         const isIgnored = _.includes(ignoreFields, key);
  //         if (isLabelField) {
  //           return [key, formulaLabel];
  //         }

  //         if (isIgnored) {
  //           return [key, void 0];
  //         }

  //         const column = toColumnName(index + 1);
  //         const endIndex = startIndex + this._data.length - 1;
  //         const formula = `=SUM(${column}${startIndex}:${column}${endIndex})`;

  //         const reducedValue = _.chain(this._data)
  //           .map(key)
  //           .reduce((prev, cur) => prev + (cur || 0), 0)
  //           .value();

  //         return _.isNumber(reducedValue)
  //           ? [key, { formula, result: reducedValue }]
  //           : void 0;
  //       })
  //       .filter(Boolean)
  //       .uniqWith(([key1], [key2]) => key1 === key2)
  //       .fromPairs()
  //       .value();

  //     this._data = [...this._data, formulaRow];

  //     if (formulaLabel) {
  //       const fields = _.chain(formulaRow)
  //         .toPairs()
  //         .filter(([field, value]) => value === formulaLabel)
  //         .map(([field]) => field)
  //         .uniq()
  //         .value();

  //       const firstIndex = _.findIndex(columns, ({ key }) =>
  //         _.includes(fields, key)
  //       );
  //       const lastIndex = _.findLastIndex(columns, ({ key }) =>
  //         _.includes(fields, key)
  //       );

  //       if (firstIndex !== -1 && lastIndex !== -1) {
  //         const mergeConfig = {
  //           lineStart: startIndex + this._data.length - 2,
  //           lineEnd: startIndex + this._data.length - 2,
  //           columnStart: firstIndex,
  //           columnEnd: lastIndex
  //         };

  //         this._additionalMerges.push(mergeConfig);
  //       }
  //     }
  //   }
  // }

  public generate() {
    const wb = new Excel.stream.xlsx.WorkbookWriter(this.excelOptions);
    const sh = wb.addWorksheet(this.name);

    const { excelConfigs } = this.content;
    sh.columns = excelConfigs;

    const dataStartIndex = _.map(
      this.headers,
      ({ data }) => data.length
    ).reduce((x, y) => x + y, 1);

    const base = _.chain([...this.headers, this.content])
      .map(({ data }) => data)
      .flatten()
      .value();

    // this._calculateFormulaRow(columns, dataStartIndex);
    _.each(base, sh.addRow.bind(sh));

    this.data.forEach(row => {
      sh.addRow(
        excelConfigs.map(({ key }) =>
          _.isNaN(_.get(row, key, '')) ? '' : _.get(row, key, '')
        )
      );
    });

    // _.each(
    //   this._additionalMerges,
    //   ({ lineStart, lineEnd, columnStart, columnEnd }) => {
    //     sh.mergeCells(
    //       lineStart + 1,
    //       columnStart + 1,
    //       lineEnd + 1,
    //       columnEnd + 1
    //     );
    //   }
    // );

    // Merge data cells
    // The same columns
    _.each(excelConfigs, (column, index) => {
      if (index && excelConfigs[index - 1] === column) {
        return true;
      }

      const nextColumnIndex = _.findIndex(
        excelConfigs,
        elem => elem !== column,
        index
      );
      const columnEndIndex =
        nextColumnIndex === -1 ? excelConfigs.length : nextColumnIndex;

      if (columnEndIndex - 1 > index) {
        for (let i = 0; i < this.data.length; i++) {
          try {
            sh.mergeCells(
              i + dataStartIndex,
              index + 1,
              i + dataStartIndex,
              columnEndIndex
            );
          } catch (err) {
            console.log([
              i + dataStartIndex,
              index + 1,
              i + dataStartIndex,
              columnEndIndex
            ]);
          }
        }
      }

      return true;
    });

    const footersData = _.chain(this.footers)
      .map(({ data }) => data)
      .flatten()
      .value();

    _.each(footersData, sh.addRow.bind(sh));

    let startIndex = 0;

    const configs = _.chain([...this.headers, this.content, ...this.footers])
      .map(({ getRange, data, getExcelConfigs }) => {
        const ranges = getRange(startIndex);
        const excelCellConfigs = getExcelConfigs(startIndex);

        startIndex += data.length;

        return { ranges, excelCellConfigs };
      })
      .value();

    const merges = _.chain(configs)
      .map(({ ranges }) => ranges)
      .flatten()
      .value();

    const excelCellConfigs = _.chain(configs)
      .map(({ excelCellConfigs }) => excelCellConfigs)
      .flatten()
      .value();

    _.each(excelCellConfigs, ({ line, column, cellConfig }) => {
      _.assign(sh.getCell(line + 1, column + 1), cellConfig);
    });

    _.each(merges, ({ lineStart, lineEnd, columnStart, columnEnd }) => {
      try {
        sh.mergeCells(
          lineStart + 1,
          columnStart + 1,
          lineEnd + 1,
          columnEnd + 1
        );
      } catch (err) {
        console.log(err);
      }
    });

    sh.eachRow(row => {
      row.font = font;
      row.height = 20.5;
    });

    sh.commit();
    wb.commit();

    return wb;
  }
}
