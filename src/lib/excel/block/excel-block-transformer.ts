import _ from 'lodash';

import { Column } from 'exceljs';
import { BlockDefinition } from './block-definition.interface';
import {
  BlockRange,
  ExcelBlockCellConfig,
  ExcelBlockDefinition
} from './excel-block-definition.interface';

type StackContext = Pick<
  BlockDefinition,
  'value' | 'config' | 'excelConfig' | 'excelCellConfig'
>;

interface StackBlockDefinition extends StackContext {
  stack: StackContext[];
}

/**
 * Create new config structure in order to build headers more easily regarding hierarchy
 * @param {*} config contains three properties: value, children, config; defines hierarchal structure
 * @param {*} stack parent nodes
 */
const transform = (
  blockDefinition: BlockDefinition,
  stack: StackContext[] = []
): StackBlockDefinition[] => {
  const {
    value,
    children,
    excelConfig,
    excelCellConfig,
    config
  } = blockDefinition;

  const currentStack: StackContext[] = [
    ...stack,
    { value, excelConfig, config, excelCellConfig }
  ];

  if (children) {
    const result = _.map(children, child => transform(child, currentStack));
    return _.flattenDepth(result) as StackBlockDefinition[];
  }

  const mergeWidth: number = config ? config.mergeWidth || 1 : 1;

  return Array(mergeWidth).fill({
    stack: currentStack,
    config,
    excelConfig,
    excelCellConfig
  });
};

export const getExcelBlockDefinitions = (
  blockDefinitions: BlockDefinition[]
): ExcelBlockDefinition => {
  // Transform data to the array with pairs { value, stack }
  const normalizedData = _.chain(blockDefinitions)
    .map(item => transform(item))
    .flatten()
    .value();

  // Retract exceljs configs
  const excelConfigs: Column[] = _.map(
    normalizedData,
    ({ excelConfig }) => excelConfig
  ).filter(Boolean) as Column[];

  const base: Array<Array<string | null | undefined>> = [];

  // fill the values
  _.each(normalizedData, ({ stack: path }, columnIndex) => {
    const deltaRows = path.length - base.length;

    for (let i = 0; i < deltaRows; i++) {
      base.push(Array(normalizedData.length).fill(void 0));
    }

    _.each(path, ({ value }, rowIndex) => {
      base[rowIndex][columnIndex] = value;
    });
  });

  const getExcelConfigs = (initRow = 0) => {
    const excelConfigs: ExcelBlockCellConfig[] = [];
    _.each(normalizedData, ({ stack: path }, ndIndex) => {
      _.each(path, ({ excelCellConfig }, pathIndex) => {
        if (excelCellConfig) {
          excelConfigs.push({
            cellConfig: excelCellConfig,
            line: pathIndex + initRow,
            column: ndIndex
          });
        }
      });
    });

    return excelConfigs;
  };

  const getRange = (initRow = 0) => {
    const ranges: BlockRange[] = [];

    _.each(base, (row, rowIndex) => {
      _.each(row, (value, columnIndex) => {
        // Check if cell is already in merge area
        const existsInRange = _.find(
          ranges,
          ({ lineStart, lineEnd, columnEnd, columnStart }) =>
            lineStart <= rowIndex + initRow &&
            lineEnd >= rowIndex + initRow &&
            columnStart <= columnIndex &&
            columnEnd >= columnIndex
        );

        if (existsInRange) {
          return true;
        }

        // Find start, end positions
        // Calculate next column index
        const findIndexResult = _.findIndex(
          row,
          elem => elem !== value,
          columnIndex
        );
        const nextColumnIndex =
          findIndexResult === -1 ? row.length : findIndexResult;

        // Calculate next row index
        let nextRowIndex = rowIndex + 1;
        for (; nextRowIndex < base.length; nextRowIndex++) {
          const nextRow = base[nextRowIndex].slice(
            columnIndex,
            nextColumnIndex
          );
          const isInvalid = _.find(nextRow, elem => elem !== value);

          if (isInvalid) {
            break;
          }
        }

        // Push result if an area exist
        if (nextColumnIndex - columnIndex > 1 || nextRowIndex - rowIndex > 1) {
          ranges.push({
            lineStart: rowIndex + initRow,
            lineEnd: nextRowIndex + initRow - 1,
            columnStart: columnIndex,
            columnEnd: nextColumnIndex - 1
          });
        }

        return true;
      });
    });

    return ranges;
  };
  return { data: base, getRange, getExcelConfigs, excelConfigs };
};
