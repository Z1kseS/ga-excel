import { Cell, Column } from 'exceljs';

export interface BlockDefinitionConfig {
  mergeWidth?: number;
}

export interface BlockDefinition {
  value: string | null | undefined;
  config?: BlockDefinitionConfig;
  children?: BlockDefinition[];
  excelConfig?: Partial<Column>;
  excelCellConfig?: Partial<Cell>;
}
