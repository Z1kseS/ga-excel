import _ from 'lodash';

import {
  IterationResult,
  IterationResultStats,
  statsFields
} from '../interfaces/iteration-result.interface';

import { DIMENSIONS } from '../constants';
import { aggregate } from '../utils/aggregator';
import { avgFieldsAggregator } from '../utils/avg-aggregator';
import { maxFieldsAggregator } from '../utils/max-aggregator';
import { successFilter } from '../utils/success_filter';

class Transformer {
  public getAggregatedIterations(
    iterationResults: IterationResult[],
    iterationFields: string[],
    maxDim: number
  ): Array<{}> {
    const criteriaFields = _.difference(iterationFields, [
      ...statsFields,
      'evaluation',
      'run',
      'dimension'
    ]) as Array<
      Exclude<
        keyof IterationResult,
        keyof IterationResultStats | 'evaluation' | 'run' | 'dimension'
      >
    >;

    const criteriaResults = aggregate(iterationResults, criteriaFields).map(
      ({ key, data }) => {
        return { key, data: aggregate(data, ['evaluation']) };
      }
    );

    return criteriaResults.map(({ key, data }) => {
      const allData = _.flatten(_.map(data, 'data'));
      const successAllData = successFilter(allData);

      const oneNData = allData.filter(({ dimension }) => dimension <= maxDim);
      const successOneNData = successFilter(oneNData);

      const dims = DIMENSIONS.filter(dim => dim <= maxDim).map(value => {
        const successAllDimData = successAllData.filter(
          ({ dimension }) => dimension === value
        );
        const allDimData = allData.filter(
          ({ dimension }) => dimension === value
        );

        return [
          'dim' + value,
          {
            avg: avgFieldsAggregator(successAllDimData, statsFields),
            max: maxFieldsAggregator(successAllDimData, statsFields),
            sucRuns: successAllDimData.length / allDimData.length
          }
        ];
      });

      return {
        key,
        avg: avgFieldsAggregator(successOneNData, statsFields),
        max: maxFieldsAggregator(successOneNData, statsFields),
        all: _.fromPairs(dims),
        sucRuns: successOneNData.length / oneNData.length,
        data: _.fromPairs([
          ...data.map(runData => {
            const dimsEvaluations = DIMENSIONS.filter(dim => dim <= maxDim).map(
              value => {
                const dimData = runData.data.filter(
                  ({ dimension }) => dimension === value
                );
                const successDimData = successFilter(dimData);

                return [
                  'dim' + value,
                  {
                    avg: avgFieldsAggregator(successDimData, statsFields),
                    max: maxFieldsAggregator(successDimData, statsFields),
                    sucRuns: successDimData.length / dimData.length
                  }
                ];
              }
            );

            return [runData.key.evaluation, _.fromPairs(dimsEvaluations)];
          })
        ])
      };
    });
  }
}

export const allTransformer = new Transformer();
