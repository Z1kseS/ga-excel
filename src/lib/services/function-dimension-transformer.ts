import _ from 'lodash';

import {
  IterationResult,
  IterationResultStats,
  statsFields
} from '../interfaces/iteration-result.interface';
import {
  fileFields,
  FileKey,
  FileResult
} from '../interfaces/reports/function-dimension-report.interface';

import { aggregate } from '../utils/aggregator';
import { avgFieldsAggregator } from '../utils/avg-aggregator';
import { maxFieldsAggregator } from '../utils/max-aggregator';
import { successFilter } from '../utils/success_filter';

class Transformer {
  public getAggregatedIterations(
    iterationResults: IterationResult[],
    iterationFields: string[]
  ): FileResult[] {
    const criteriaFields = _.difference(iterationFields, [
      ...fileFields,
      ...statsFields,
      'run'
    ]) as Array<
      Exclude<
        keyof IterationResult,
        keyof FileKey | keyof IterationResultStats | 'run'
      >
    >;

    const fileResults: FileResult[] = aggregate<IterationResult, keyof FileKey>(
      iterationResults,
      fileFields
    ).map(({ key, data }) => {
      const result = aggregate(data, criteriaFields);
      const sortedRuns = result.map(({ key: sortKey, data: sortData }) => {
        const sortedData = Array(10).fill(void 0); // 10 iterations

        sortData.forEach(value => {
          if (value.run <= 9 && value.run >= 0) {
            _.set(sortedData, value.run, value);
          }
        });

        return { key: sortKey, data: sortedData };
      });

      return { key, data: sortedRuns };
    });

    const finalData = fileResults.map(runData => ({
      ...runData,
      data: runData.data.map(statsData => ({
        ...statsData,
        avg: avgFieldsAggregator(
          successFilter(statsData.data).filter(Boolean),
          statsFields
        ),
        max: maxFieldsAggregator(
          successFilter(statsData.data).filter(Boolean),
          statsFields
        ),
        sucRuns:
          successFilter(statsData.data).length /
          statsData.data.filter(Boolean).length
      }))
    }));

    return finalData;
  }
}

export const functionDimensionTransformer = new Transformer();
