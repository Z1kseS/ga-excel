import { Borders, stream } from 'exceljs';
import _ from 'lodash';

import {
  BlockDefinition,
  BlockDefinitionConfig
} from '../excel/block/block-definition.interface';
import { ExcelContentBlockDefinition } from '../excel/block/excel-block-definition.interface';
import { getExcelBlockDefinitions } from '../excel/block/excel-block-transformer';
import { Composer } from '../excel/composer/composer';
import { statsFields } from '../interfaces/iteration-result.interface';
import { fileFields } from '../interfaces/reports/function-dimension-report.interface';
import { collector } from '../services/collector';
import { functionDimensionTransformer } from '../services/function-dimension-transformer';

const border: Borders = {
  top: { style: 'thin' },
  left: { style: 'thin' },
  bottom: { style: 'thin' },
  right: { style: 'thin' },
  diagonal: {}
};

interface Property {
  prop: string;
  key: string;
  config?: BlockDefinitionConfig;
}

const configGenerator: (
  configuration: Property[],
  sections: Property[],
  criteria: Property[]
) => BlockDefinition[] = (configuration, sections, criteria) => [
  {
    value: 'Критерій',
    excelCellConfig: { style: { border } },
    children: configuration.map(({ prop, key: propKey, config }) => ({
      value: prop,
      config,
      excelCellConfig: { style: { border } },
      excelConfig: {
        key: `key.${propKey}`
      }
    }))
  },
  ...sections.map(({ prop: sectionProp, key: sectionKey }) => ({
    value: sectionProp,
    excelCellConfig: { style: { border } },
    children: criteria.map(({ prop: criterionProp, key: criterionKey }) => ({
      value: criterionProp,
      excelCellConfig: { style: { border } },
      excelConfig: {
        key: `${sectionKey}.${criterionKey}`
      }
    }))
  })),
  {
    value: 'SucRuns',
    excelCellConfig: { style: { border } },
    excelConfig: { key: 'sucRuns' }
  }
];

const excelOptions = {
  useStyles: true,
  dateFormats: ['DD/MM/YYYY']
};

export interface FunctionReport {
  name: string;
  stream: stream.xlsx.WorkbookWriter;
}

export const functionDimensionReport = (path: string): FunctionReport[] => {
  const iterationResults = collector.collect(path);
  const iterationFields = _.keys(_.first(iterationResults));

  const configuration = _.difference(iterationFields, [
    ...fileFields,
    ...statsFields,
    'run'
  ]).map(name => ({ prop: name, key: name }));

  const criteria: Property[] = [
    ...statsFields.map(name => ({ prop: name, key: name }))
  ];

  const sections = [
    { prop: 'Прогін 1', key: 'data[0]' },
    { prop: 'Прогін 2', key: 'data[1]' },
    { prop: 'Прогін 3', key: 'data[2]' },
    { prop: 'Прогін 4', key: 'data[3]' },
    { prop: 'Прогін 5', key: 'data[4]' },
    { prop: 'Прогін 6', key: 'data[5]' },
    { prop: 'Прогін 7', key: 'data[6]' },
    { prop: 'Прогін 8', key: 'data[7]' },
    { prop: 'Прогін 9', key: 'data[8]' },
    { prop: 'Прогін 10', key: 'data[9]' },
    { prop: 'Середнє', key: 'avg' },
    { prop: 'Найкраще', key: 'max' }
  ];

  const data = functionDimensionTransformer.getAggregatedIterations(
    iterationResults,
    iterationFields
  );

  const headers: BlockDefinition[][] = [];
  const footers: BlockDefinition[][] = [];

  const content = getExcelBlockDefinitions(
    configGenerator(configuration, sections, criteria)
  ) as ExcelContentBlockDefinition;

  const arr: FunctionReport[] = data.map(subData => {
    const composer = new Composer(
      'report',
      headers.map(getExcelBlockDefinitions),
      content,
      footers.map(getExcelBlockDefinitions),
      subData.data,
      excelOptions
    );

    return {
      name: `T_${subData.key.evaluation}_${subData.key.dimension}.xlsx`,
      stream: composer.generate()
    };
  });

  return arr;
};
