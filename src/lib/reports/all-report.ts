import { Borders, stream } from 'exceljs';
import _ from 'lodash';

import {
  BlockDefinition,
  BlockDefinitionConfig
} from '../excel/block/block-definition.interface';
import { ExcelContentBlockDefinition } from '../excel/block/excel-block-definition.interface';
import { getExcelBlockDefinitions } from '../excel/block/excel-block-transformer';
import { Composer } from '../excel/composer/composer';
import { statsFields } from '../interfaces/iteration-result.interface';
import { fileFields } from '../interfaces/reports/function-report.interface';
import { allTransformer } from '../services/all-transformer';
import { collector } from '../services/collector';

const border: Borders = {
  top: { style: 'thin' },
  left: { style: 'thin' },
  bottom: { style: 'thin' },
  right: { style: 'thin' },
  diagonal: {}
};

interface Property {
  prop: string;
  key: string;
  config?: BlockDefinitionConfig;
}

const configGenerator: (
  functions: Property[],
  configuration: Property[],
  sections: Property[],
  criteria: Property[]
) => BlockDefinition[] = (functions, configuration, sections, criteria) => [
  {
    value: 'Критерій',
    excelCellConfig: { style: { border } },
    children: configuration.map(({ prop, key: propKey, config }) => ({
      value: prop,
      config,
      excelCellConfig: { style: { border } },
      excelConfig: {
        key: `key.${propKey}`
      }
    }))
  },
  ...functions.map(({ prop: functionProp, key: functionKey }) => ({
    value: functionProp,
    excelCellConfig: { style: { border } },
    children: sections.map(({ prop: sectionProp, key: sectionKey }) => ({
      value: sectionProp,
      excelCellConfig: { style: { border } },

      children: [
        ...['avg', 'max'].map(name => ({
          value: name,
          excelCellConfig: { style: { border } },

          children: criteria.map(
            ({ prop: criterionProp, key: criterionKey }) => ({
              value: criterionProp,
              excelCellConfig: { style: { border } },
              excelConfig: {
                key: `data.${functionKey}.${sectionKey}.${name}.${criterionKey}`
              }
            })
          )
        })),
        {
          value: 'SucRuns',
          excelCellConfig: { style: { border } },
          excelConfig: {
            key: `data.${functionKey}.${sectionKey}.sucRuns`
          }
        }
      ]
    }))
  })),
  {
    value: 'All functions',
    excelCellConfig: { style: { border } },
    children: sections.map(({ prop: sectionProp, key: sectionKey }) => ({
      value: sectionProp,
      excelCellConfig: { style: { border } },

      children: [
        ...['avg', 'max'].map(name => ({
          value: name,
          excelCellConfig: { style: { border } },

          children: criteria.map(
            ({ prop: criterionProp, key: criterionKey }) => ({
              value: criterionProp,
              excelCellConfig: { style: { border } },
              excelConfig: {
                key: `all.${sectionKey}.${name}.${criterionKey}`
              }
            })
          )
        })),
        {
          value: 'SucRuns',
          excelCellConfig: { style: { border } },
          excelConfig: {
            key: `all.${sectionKey}.sucRuns`
          }
        }
      ]
    }))
  },
  ...['avg', 'max'].map(name => ({
    value: name,
    excelCellConfig: { style: { border } },

    children: criteria.map(({ prop: criterionProp, key: criterionKey }) => ({
      value: criterionProp,
      excelCellConfig: { style: { border } },
      excelConfig: {
        key: `${name}.${criterionKey}`
      }
    }))
  })),
  {
    value: 'Suc Runs',
    excelCellConfig: { style: { border } },
    excelConfig: { key: 'sucRuns' }
  }
];

const excelOptions = {
  useStyles: true,
  dateFormats: ['DD/MM/YYYY']
};

export interface FunctionReport {
  name: string;
  stream: stream.xlsx.WorkbookWriter;
}

export const allReport = (path: string, maxDim: number): FunctionReport[] => {
  const initIterationResults = collector.collect(path);
  const iterationResults = initIterationResults.map(iterationResult => ({
    ...iterationResult,
    evaluation: iterationResult.evaluation.replace('.', '_')
  }));

  const iterationFields = _.keys(_.first(iterationResults));
  const functions = _.uniq(_.map(iterationResults, 'evaluation')).map(name => ({
    prop: name,
    key: name
  }));

  const configuration = _.difference(iterationFields, [
    ...fileFields,
    ...statsFields,
    'dimension',
    'run'
  ]).map(name => ({ prop: name, key: name }));

  const criteria = [...statsFields.map(name => ({ prop: name, key: name }))];

  const sections = [1, 2, 3, 4, 5, 10, 15, 20]
    .filter(dim => dim <= maxDim)
    .map(value => ({ prop: 'Dim ' + value, key: 'dim' + value }));

  const data = allTransformer.getAggregatedIterations(
    iterationResults,
    iterationFields,
    maxDim
  );

  const headers: BlockDefinition[][] = [];
  const footers: BlockDefinition[][] = [];

  const content = getExcelBlockDefinitions(
    configGenerator(functions, configuration, sections, criteria)
  ) as ExcelContentBlockDefinition;

  const composer = new Composer(
    'report',
    headers.map(getExcelBlockDefinitions),
    content,
    footers.map(getExcelBlockDefinitions),
    data,
    excelOptions
  );

  return [
    {
      name: `Summary${maxDim}.xlsx`,
      stream: composer.generate()
    }
  ];
};
